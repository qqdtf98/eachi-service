package com.demo.microservices.recommend.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.demo.microservices.recommend.model.RcmdUser;
import com.demo.microservices.recommend.service.RecommendService;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Controller
public class RecommendController {

	private final RecommendService recommendService;

	@Value("${garage.pageSize}")
	private int pageSize;

    /**
     * 추천상품 목록 조회
     */
	@ApiOperation(value="맞춤 상품 Top3")
    @PostMapping("/")
    public @ResponseBody Object recommendList(@RequestBody RcmdUser user) {
		
		log.info("start ***********");	
        return recommendService.findAll(user);
	}

}
