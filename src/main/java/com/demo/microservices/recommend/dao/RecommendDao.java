package com.demo.microservices.recommend.dao;

import java.util.List;
import java.util.Map;

public interface RecommendDao {

	public <T> List<T> selectList(String id, Map<String, Object> map) throws Exception;

}
