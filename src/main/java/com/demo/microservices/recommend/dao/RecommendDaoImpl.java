package com.demo.microservices.recommend.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class RecommendDaoImpl implements RecommendDao {
	@Autowired
	private SqlSession sqlSession;

	@Override
	public <T> List<T> selectList(String id, Map<String, Object> map) throws Exception {
		List<T> list = null;
		try {
			list = sqlSession.selectList(id, map);
		} catch (Exception e) {
			log.error("[ERROR]", e);

			throw e;
		}
		return list;
	}

}
