package com.demo.microservices.recommend.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RcmdUser {
	 private String  birth 		; // 사용자 생년월일
	 private String  gender		; // 사용자 성별
}
