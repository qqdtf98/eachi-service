package com.demo.microservices.recommend.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
public class RcmdProduct {
	private Long id;
	private String name;
	private String info;
}
