package com.demo.microservices.recommend.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.microservices.recommend.dao.RecommendDao;
import com.demo.microservices.recommend.model.RcmdProduct;
import com.demo.microservices.recommend.model.RcmdUser;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RecommendService {
	@Autowired
	private RecommendDao recommendDao;

	/**
	 * 추천 상품 Top3 조회
	 * 
	 * @param search
	 * @return
	 */
	public List<RcmdProduct> findAll(RcmdUser user) {
		try {

			log.info("search:{}", user);

			List<RcmdProduct> list = null;
			Map map = new HashMap();
			
			map.put("birth", user.getBirth());
			map.put("gender", user.getGender());
			
			list = recommendDao.selectList("findTop3", map);

			log.info("product:{}", list);
			return list;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
