package com.demo.microservices.user.controller;

import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.demo.microservices.user.controller.dto.UserDto;
import com.demo.microservices.user.model.PrincipalDetail;
import com.demo.microservices.user.model.User;
import com.demo.microservices.user.service.UserService;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Controller
public class UserController {
	private final UserService userService;
	
	@ApiOperation(value="회원등록")
    @PostMapping("/auth/api/v1/user")
    public @ResponseBody Long save(@RequestBody UserDto userSaveRequestDto) {
    	log.info("=================================");
    	log.info("userSaveRequestDto:{}", userSaveRequestDto);
    	
    	User user = userSaveRequestDto.toEntity();

    	userService.save(user);
    	
        return 1L;
    }
    
    /**
     * 회원수정 API
     */
	@ApiOperation(value="회원수정")
    @PutMapping("/api/v1/user")
    public @ResponseBody Long update(@RequestBody UserDto userSaveRequestDto, @AuthenticationPrincipal PrincipalDetail principalDetail) {
    	User user = userSaveRequestDto.toEntity();
    	
    	log.info("update:{}", principalDetail);
        return userService.update(user);
    }   
	
    @GetMapping("/auth/user/login")
    public String userLogin() {
        return "layout/user/user-login";
    }
    
    
    
    /**
     * 회원수정 페이지
     */
    @GetMapping("/user/update")
    public String userUpdate() {
        return "layout/user/user-update";
    }    
	
    @GetMapping("/auth/user/save")
    public String userSave() {
        return "layout/user/user-save";
    }
}