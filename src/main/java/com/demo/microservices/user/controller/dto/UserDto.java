package com.demo.microservices.user.controller.dto;


import com.demo.microservices.user.model.User;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserDto {
    private String username;
    private String password;
    private String nickname;
    
    public User toEntity() {
    	return User.builder()
    			.username(username)
    			.password(password)
    			.nickname(nickname)
    			.build();
    }
}
