package com.demo.microservices.user.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.demo.microservices.common.dao.CommonDao;
import com.demo.microservices.user.model.Role;
import com.demo.microservices.user.model.User;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserService {

	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	
	private final CommonDao commonDao;
	
	public Long save(User user) {
		
		String bCryptPassword = bCryptPasswordEncoder.encode(user.getPassword());
		long rc = -1L;
		try {
			user.setPassword(bCryptPassword);
			user.setRole(Role.USER.getKey());
			
			rc = commonDao.insert("registUser", user);
		} catch(Exception e) {
			throw new RuntimeException(e);
		}

		return 1L;
	}
	
	public Long update(User user) {
		
		String bCryptPassword = bCryptPasswordEncoder.encode(user.getPassword());
		long rc = -1L;
		try {
			user.setPassword(bCryptPassword);
			user.setRole(Role.USER.getKey());
			
			rc = commonDao.update("upsertUser", user);
		} catch(Exception e) {
			throw new RuntimeException(e);
		}

		return 1L;
	}	
}
