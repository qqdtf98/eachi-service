package com.demo.microservices.user.service;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.demo.microservices.common.dao.CommonDao;
import com.demo.microservices.user.model.PrincipalDetail;
import com.demo.microservices.user.model.User;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserDetailService implements UserDetailsService {
	@Autowired
	CommonDao commonDao;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		try {
			log.info("===========================================");
			log.info("username:{}", username);
			log.info("===========================================");
			User user = commonDao.selectOne("selectUserRole", username);
			
			if (user == null) {
				log.error("User not found in the database");
				throw new UsernameNotFoundException("User not found in the database");
			} else {
				log.info("userRoles:{}", user.getRole());
			}
			
			return new PrincipalDetail(user);
		
		} catch (Exception e) {
			log.error("ERROR", e);
			throw new RuntimeException(e);
		}
	}

}
