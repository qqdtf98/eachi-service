package com.demo.microservices.user.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
public class User {
	private String username;
	private String password;
	private String nickname;
	private String role;
}
