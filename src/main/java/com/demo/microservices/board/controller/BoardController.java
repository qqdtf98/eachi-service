package com.demo.microservices.board.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.demo.microservices.board.controller.dto.BoardDto;
import com.demo.microservices.board.controller.dto.ReplyDto;
import com.demo.microservices.board.model.Reply;
import com.demo.microservices.board.service.BoardService;
import com.demo.microservices.user.model.PrincipalDetail;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Controller
public class BoardController {
    
    private final BoardService boardService;
    
	@Value("${garage.pageSize}")
	private int pageSize;
	
    /**
     * 게시글작성 API
     */
	@ApiOperation(value="게시글을 등록 합니다.")
    @PostMapping("/api/v1/board")
    public @ResponseBody Long save(@RequestBody BoardDto boardSaveRequestDto, @AuthenticationPrincipal PrincipalDetail principalDetail) {
        return boardService.save(boardSaveRequestDto, principalDetail.getUser());
    }
	
    
    /**
     * 게시글 삭제 API
     */
	@ApiOperation(value="게시글을 삭제 합니다.")
    @DeleteMapping("/api/v1/board/{id}")
    public @ResponseBody Long deleteById(@PathVariable Long id) {
        boardService.deleteById(id);
        return id;
    } 
    
    /**
     * 게시글 수정 API
     */
	@ApiOperation(value = "게시글을 수정합니다.")
    @PutMapping("/api/v1/board/{id}")
    public @ResponseBody Long update(@PathVariable Long id, @RequestBody BoardDto boardSaveRequestDto) {
        return boardService.update(id, boardSaveRequestDto);
    }
    
    /**
     * 게시글 댓글 등록
     * @param boardId
     * @param replyDto
     * @param principalDetail
     */
	@ApiOperation(value = "게시글 댓글을 등록합니다.")
    @PostMapping("/api/v1/board/{boardId}/reply")
    public @ResponseBody void saveReply(@PathVariable Long boardId,
                     @RequestBody ReplyDto replyDto,
                     @AuthenticationPrincipal PrincipalDetail principalDetail) {
    	
    	Reply reply = replyDto.toEntity();
    	reply.setUsername(principalDetail.getUsername());
    	boardService.replySave(boardId, reply);
    }
    
    /**
     * 댓글 삭제 
     * @param replyId
     */
	@ApiOperation(value = "게시글 댓글을 삭제합니다.")
    @DeleteMapping("/api/v1/board/{boardId}/reply/{replyId}")
    public @ResponseBody void delete(@PathVariable Long boardId,	@PathVariable Long replyId) {
		log.info("replyId:{}", replyId);
    	boardService.replyDelete(replyId);
    }
		
	@ApiOperation(value = "게시글 등록화면")
    @GetMapping("/board/save")
    public String save() {
        return "layout/board/board-save";
    }

	@ApiOperation(value = "게시글 목록조회")
    @GetMapping("/")
    public String boardList(Model model, @RequestParam(required = false, defaultValue = "") String search, @RequestParam(required = false, defaultValue = "") String page) {
    	log.info("page:{}", page);
        int pageNumber = 0;
        
        if (page == null || page.length() == 0) {
        	pageNumber = 0;
        } else {
        	pageNumber = Integer.parseInt(page);
        }
        
		int from = pageNumber * pageSize + 1;
		int to = pageNumber * pageSize + pageSize;
		Map<String, Object> parameters = new HashMap();
		
		parameters.put("from", from);
		parameters.put("to", to);
		parameters.put("search", search);
		
        model.addAttribute("boards", boardService.findAll(parameters));

        int maxRows = boardService.getTotalPage(search);


        int totalPages = maxRows / pageSize;
        
        if (maxRows % pageSize != 0) {
        	totalPages++;
        }

        log.info("pageNumber:{}, totalPages:{}", pageNumber, totalPages);
        int startPage = Math.max(1, pageNumber - 4);
        int endPage = Math.min(totalPages, pageNumber + 4);
        model.addAttribute("pageSize", 2);
        model.addAttribute("pageNumber", pageNumber);
        model.addAttribute("totalPages", totalPages);
        model.addAttribute("startPage", startPage);
        model.addAttribute("endPage", endPage);
        
        return "index";
    }
    
	@ApiOperation(value = "게시글 상세화면")
    @GetMapping("/board/{id}")
    public String detail(@PathVariable Long id, Model model) {
        model.addAttribute("board", boardService.detail(id));
        model.addAttribute("replyList", boardService.reply(id));
        boardService.updateCount(id);
        return "layout/board/board-detail";
    }  
    
	@ApiOperation(value = "게시글 수정화면")
    @GetMapping("/board/{id}/update")
    public String update(@PathVariable Long id, Model model) {
        model.addAttribute("board", boardService.detail(id));
        return "layout/board/board-update";
    }   
    
}
