package com.demo.microservices.board.controller.dto;

import com.demo.microservices.board.model.Reply;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReplyDto {
	private Long id;
	private String content;
	private Long boardId;
	private String username;
	
	public Reply toEntity() {
		return Reply.builder()
				.content(content)
				.boardId(boardId)
				.username(username)
				.build();
	}
}
