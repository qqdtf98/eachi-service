package com.demo.microservices.board.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.microservices.board.controller.dto.BoardDto;
import com.demo.microservices.board.model.Board;
import com.demo.microservices.board.model.Reply;
import com.demo.microservices.common.dao.CommonDao;
import com.demo.microservices.user.model.User;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BoardService {
	@Autowired
	private CommonDao commonDao;
	
	/**
	 * 게시글 저장 
	 * @param boardSaveRequest
	 * @param user
	 * @return
	 */
	public Long save(BoardDto boardSaveRequest, User user) {
		try {
			log.info("board:{}, user:{}", boardSaveRequest, user);
			Board board = boardSaveRequest.toEntity();
			board.setUsername(user.getUsername());
			long rc = commonDao.insert("upsertBoard", board);
			
			return rc;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
    /**
     * 게시글상세 
     */
    @Transactional(readOnly = true)
    public Board detail(Long id) {
		try {
			Board board= commonDao.selectOne("findById", id);
			log.info("board:{}", board);

			return board;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
    }

    /**
     * 게시글 상세 - 댓글
     */
    @Transactional(readOnly = true)
    public List<Reply> reply(Long id) {
		try {
			List<Reply> replyList = commonDao.selectList("findByBoardId", id);
			log.info("reply:{}", replyList);
			return replyList;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
    
    /**
     * 게시글 삭제
     */
    @Transactional
    public void deleteById(Long id) {
		try {
			log.info("board id:{}", id);
			commonDao.delete("deleteById", id);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
    
    /**
     * 게시글 조회 수 증가 
     */
    @Transactional
    public void updateCount(Long id) {
		try {
			log.info("board id:{}", id);
			commonDao.update("updateCount", id);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
    
    /**
     * 게시글 수정 
     */
    @Transactional
    public Long update(Long id, BoardDto boardSaveRequestDto) {

		try {
	        Board board = commonDao.selectOne("findById", id);
	        
	        board.setTitle(boardSaveRequestDto.getTitle());
	        board.setContent(boardSaveRequestDto.getContent());
			log.info("updated board :{}", board);
			commonDao.insert("updateBoard", board);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}       
        
        return id;
    }
    
    /**
     * 게시글 목록 조회 
     * @param search
     * @return
     */
	public List<Board> findAll(Map map) {
		try {

			log.info("search:{}",map);
			String search = (String)map.get("search");
			List<Board> boardList= null;
			
			if (search != null && search.length() > 0) {
				boardList=commonDao.selectList("findByTitleOrContent", map);
			} else {
				log.info("findAll:{}", map);
				boardList=commonDao.selectList("findAll", map);
			}

			log.info("board:{}", boardList);
			return boardList;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}	
	
	public int getTotalPage(String search) {
		int count;
		try {

			log.info("search:{}",search);
			List<Board> boardList= null;
			
			if (search != null && search.length() > 0) {
				count=commonDao.selectOne("totalCountByTitleOrContent", search);
			} else {
				count=commonDao.selectOne("totalCount");
			}

			log.info("board:{}", count);
			return count;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 댓글 등록
	 * @param boardId
	 * @param reply
	 */
    @Transactional
    public void replySave(Long boardId, Reply reply) {
		try {
	        Board board = commonDao.selectOne("findById", boardId);
			
	        reply.setBoardId(board.getId());
	        
			commonDao.insert("insertReply", reply);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
    
    /**
     * 댓글 삭제
     * @param replyId
     */
    @Transactional
    public void replyDelete(Long replyId) {
		try {
	        long rc = commonDao.delete("deleteByReplyId", replyId);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
    }	
}
