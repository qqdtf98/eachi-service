package com.demo.microservices.board.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
public class Board {
	private Long id;
	private String title;
	private String content;
	private int count;
	private String username;
	private String createdDate;
	private String modifiedDate;
}
