'use strict';

let index = {
	init: function () {
		$("#get-ins-btn").on("click",()=> {
			this.getRecomList();
		})
		$(".btn-design").on("click", (e)=> {
			this.setGenderValue(e);
		})
		
		
        $("#btn-save").on("click", () => { //this를 바인딩하기 위해 화샬표 함수 사용
            this.save();
        });
        
        $("#btn-update").on("click", () => {
           this.update();
        });
    },
    
    birthValue: '',
    genderValue: null,
    
    setGenderValue: function(e){
		let btnList = $(".btn-design")
		if(e.target.innerText === '남성'){
			btnList[0].style.border = "1px solid #dc3545" ;
			btnList[1].style.border = "1px solid gray" ;
			this.genderValue = 1
		} else if(e.target.innerText === '여성'){
			btnList[1].style.border = "1px solid #dc3545" ;
			btnList[0].style.border = "1px solid gray" ;
			this.genderValue = 2
		}
	},

	getRecomList: function() {
		let birthInput = $(".input-design")[0]
		this.birthValue = birthInput.value;
		
		let recomView = $("#ins-recom-list")
		recomView.css("display","flex")
		
		console.log(this.birthValue.replace(/\./g,''));
		console.log(this.genderValue);
		
		// api 호출
	},
	
    save: function() {
        let data = { //JavaScript Object
            username: $("#username").val(),
            password: $("#password").val(),
            nickname: $("#nickname").val()
        }

        $.ajax({
            type: "POST", //Http method
            url:  "/auth/api/v1/user", //API 주소
            data: JSON.stringify(data), //JSON으로 변환
            contentType: "application/json; charset=utf-8", //MIME 타입
            dataType: "json" //응답 데이터
        }).done(function(res) {
            alert("회원가입이 완료되었습니다.");
            location.href =  "/";
        }).fail(function(err) {
            alert(JSON.stringify(err));
        });
    },
    
    update: function () {
        let data = {
            username: $("#username").val(),
            password: $("#password").val(),
            nickname: $("#nickname").val()
        }

        $.ajax({
            type: "PUT",
            url: "/api/v1/user",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        }).done(function (res) {
            alert("회원수정이 완료되었습니다.");
            location.href =  "/";
        }).fail(function (err) {
            alert(JSON.stringify(err));
        });
    }
}
index.init();

