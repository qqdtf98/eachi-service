create database demo default CHARACTER SET utf8 COLLATE utf8_general_ci;

create user 'user00'@'%' identified by 'admin1234';

grant all privileges on demo.* to 'user00'@'%';

DROP TABLE IF EXISTS tb_user;

CREATE TABLE tb_user  (
    username     varchar(20) NOT NULL,
    password    varchar(60) NOT NULL,
    nickname    varchar(100) NULL,
    ROLE        varchar(100),
    primary key (username)
);

DROP TABLE IF EXISTS board;

create table board (
	id int auto_increment not null,
	title varchar(1000) not null,
	content varchar(4000) not null,
	count int,
	username varchar(20),
	created_date varchar(14),
	modified_date varchar(14),
	primary key(id)
);

DROP TABLE IF EXISTS reply;

create table reply (
	id int auto_increment not null,
	content varchar(4000) not null,
	board_id int,
	username varchar(20),
	primary key(id)
);